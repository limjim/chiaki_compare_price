# encoding: utf-8

require 'active_record'
require 'nokogiri'
require 'open-uri'
require 'net/http'
require 'json'

ActiveRecord::Base.establish_connection(
      :adapter  => 'mysql2',
      :database => 'chiaki_v2',
      :username => 'root',
      :password => '123@123',
      :host     => '192.168.1.172'
)

class SiteCompareTarget < ActiveRecord::Base
  self.table_name = 'chi_site_compare_target'
end

class CategoryNSite < ActiveRecord::Base
  self.table_name = 'chi_category_n_site'
  belongs_to :site_compare_target
end

class ProductNCategory < ActiveRecord::Base
  self.table_name = 'chi_product_n_category'
  belongs_to :product
end

class Product < ActiveRecord::Base
  self.table_name = 'chi_product'
end

class ProductNSite < ActiveRecord::Base
  self.table_name = 'chi_product_n_site'
end

class Crawler
  def initialize
  end

  def search_google_get_link_target(keyword, domain)
    begin
      url = 'https://www.googleapis.com/customsearch/v1'
      uri = URI.parse(url)
      args = {
        key: 'AIzaSyB2jI9zoHSl9PQmDX1nksvm-qx9r97vIOw',
        cx: '000435610024944585305:5svy9ygc_qu',
        q: keyword,
        fields: 'items(link,displayLink,title)',
        googleHost: 'google.com.vn',
        siteSearch: domain
      }
      
      uri.query = URI.encode_www_form(args)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
      response_json = JSON.parse(response.body)
      hash = response_json.to_hash
      
      return hash['items']
    rescue Exception => msg
      puts msg
    end
  end

  def main
    category_n_sites = CategoryNSite.joins(:site_compare_target).where("chi_site_compare_target.is_disable" =>[0, nil])
    category_n_sites.each do |cs|
      site_id = cs.site_compare_target.id.to_i
      site_domain = cs.site_compare_target.domain.to_s
      dom = cs.site_compare_target.dom.to_s
      extract_by_method = cs.site_compare_target.extract_by_method.to_s
      product_n_categories = ProductNCategory.joins(:product).where('category_id' => cs.category_id.to_i).where("chi_product.enable_compare" => 1)
      product_n_categories.each do |pc|
        product_id = pc.product.id.to_i
        title = pc.product.title.to_s
        check_product_n_site = ProductNSite.where(:site_id => site_id, :product_id => product_id)
        
        if check_product_n_site.empty? then
          links = self.search_google_get_link_target(title, site_domain)
          links.each do |link| 
            crawl_url = link["link"]
            price = self.crawler_price_link_target(crawl_url, dom, extract_by_method)
            if price > 0 then
              new_product_n_site = ProductNSite.create(:product_id => product_id, :site_id => site_id, :price_result => price, :crawl_url => crawl_url)
              new_product_n_site.save
              break
            end
          end
        else
          flag = false
          check_product_n_site.each do |cps|
            crawl_url = cps.crawl_url.to_s
            price = self.crawler_price_link_target(crawl_url, dom, extract_by_method)
            if !nil? then
              cps.price_result = price
              cps.save
            else
              flag = true
            end
          end
          if flag then
            check_product_n_site.destroy
          end
        end
        exit
      end
    end
  end
  
  def crawler_price_link_target(url, dom, extract_by_method)
    begin
      doc = Nokogiri::HTML(open(url))
      price = 0
      doc.css(dom).each do |node|
        if extract_by_method == 'extract_by_k' then
          price = self.extract_price_by_k(node.content)
        end
        
        if extract_by_method == 'extract_by_default' then
          price = self.extract_price_by_default(node.content)
        end
        
        if extract_by_method == 'extract_by_option' then
          price = self.extract_price_by_option(node.content)
        end
      end
      return price
    rescue Exception => msg
      puts msg
    end
  end
  
  def extract_price_by_k(str)
    strPrice = str.gsub('.', '').gsub(',', '')
    strPrice = strPrice.match(/\d+k/)
    price = 0
    if (strPrice =~ /k/).nil? then
      strPrice = strPrice.to_s
      strPrice = strPrice.gsub('k', '')
      strPrice = strPrice + '000'
      price = strPrice.to_i
    end
    return price
  end

  def extract_price_by_default(str)
    strPrice = str.gsub(/[^\d]/, '')
    price = strPrice.to_i
    return price
  end
  
  def extract_price_by_option(str)
    strPrice = str.gsub(',', '').gsub('.', '').gsub(/\s+/, '')
    strPrice = strPrice.downcase
    price = 0
    
    if strPrice.match(/vnd/i) then
      strPrice = strPrice.match(/\d+vnd/)
      strPriceGoal = strPrice.to_s
      strPriceGoal = strPriceGoal.gsub('vnd', '')
      price = strPriceGoal.to_i
    elsif  strPrice.match(/đ/i) then
      strPrice = strPrice.match(/\d+đ/)
      strPriceGoal = strPrice.to_s
      strPriceGoal = strPriceGoal.gsub('đ', '')
      price = strPriceGoal.to_i
    elsif strPrice.match(/vnđ/i) then
      strPrice = strPrice.match(/\d+vnđ/)
      strPriceGoal = strPrice.to_s
      strPriceGoal = strPriceGoal.gsub('vnđ', '')
      price = strPriceGoal.to_i
    end

    return price
  end

end

c = Crawler.new
c.main
